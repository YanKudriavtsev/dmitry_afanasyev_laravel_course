<?php

use Illuminate\Database\Seeder;
use App\Models\Post;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Post::create(
            [
                'title' => 'First post',
                'slug' => 'first-post',
                'excerpt' => 'First post. Short content',
                'content' => 'First post. Content',
                'published' => true,
                'published_at' => DB::raw('CURRENT_TIMESTAMP')
            ]
        );

        Post::create(
            [
                'title' => 'Second post',
                'slug' => 'second-post',
                'excerpt' => 'Second post. Short content',
                'content' => 'Second post. Content',
                'published' => false,
                'published_at' => DB::raw('CURRENT_TIMESTAMP')
            ]
        );
    }
}
