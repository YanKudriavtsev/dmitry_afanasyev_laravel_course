@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            @foreach($posts as $post)
                <h2>{{ $post->title }}</h2>
                <p>{!! $post->excerpt  !!}</p>
                <span>Published: {{ $post->published_at }}</span>
            @endforeach
        </div>
    </div>
</div>
@endsection
